#!/usr/bin/env python

import theano
import theano.tensor as T
import numpy as np
import time

class NN:
	def __init__(self,):
		
		self.w = np.random.random((4,10))

	def step(self, inp):
		return np.dot(self.w, inp)
		
if __name__ == "__main__":
	n = NN()
	#compiled step function
	t= time.time()
	for _ in xrange(0,100000):
		inp = np.random.random(10)
		#run compiled
		n.step(inp)
	print time.time() - t

