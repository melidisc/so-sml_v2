import numpy as np
from numpy import linalg as LA

import theano
import theano.tensor as TT
from theano.ifelse import ifelse

np.random.seed(seed=0xbeef)
# from theano.tensor.shared_randomstreams import RandomStreams
# rng = RandomStreams(seed=0xbeef)

class hm_run():
		def __init__(self,c_inp_s, c_out_s, w_inp_s, w_out_s):
			#init variables
			self.c_inp_s = c_inp_s
			self.c_out_s = c_out_s
			self.w_inp_s = w_inp_s
			self.w_out_s = w_out_s
			#Controller variables
			self.controler_weights = theano.shared(
					np.random.uniform(.50001,1.5001,(c_out_s,c_inp_s))
					# np.identity(c_out_s)-np.random.uniform(10.,10.5,(c_out_s,c_inp_s))
					)
			self.controler_bias = theano.shared(
					.1*np.ones(c_out_s).astype(theano.config.floatX)
					)
			self.controler_lr = TT.scalar(name="controler lr")
			self.controler_out = TT.vector(name="controler output")
			#World variables
			self.world_weights = theano.shared(
					np.random.uniform(1.,2.,(w_out_s, w_inp_s))
					)
			self.world_bias = theano.shared(
					np.ones(w_out_s).astype(theano.config.floatX)
					)
			self.world_lr = TT.scalar(name="world lr")

			#Environment variables
			self.world_out = TT.vector(name="world output")
			self.sensors_t = TT.vector(name="sensors")
			self.motors_t = TT.vector(name="motors")
			self.sensors_tp1 = TT.vector(name="sensors tp1")
			self.real_sensors_tp1 = TT.vector(name="real sensors tp1")

			#Inverse
			self.inverse =  TT.nlinalg.MatrixInverse()
			self.determinant = TT.nlinalg.Det()
			self.diag = TT.nlinalg.AllocDiag()
			self.K_delta = self.diag(np.ones(c_out_s))
		def controler_step(self,cin):

			self.controler_out =  TT.tanh(
				TT.dot(self.controler_weights, cin) + self.controler_bias
				)

			return self.controler_out
		def world_step(self,win):

			self.world_out =  TT.dot(self.world_weights, win) + self.world_bias

			return self.world_out

		def controler_step_f(self,):
			output = self.controler_step(self.sensors_t)
			return  theano.function(
				inputs=[self.sensors_t],
				outputs=[output])

		def world_step_f(self,):
			output = self.world_step(self.motors_t)

			return theano.function(
				inputs=[self.motors_t],
				outputs=[output])

		def run(self,):
			self.world_out = self.world_step(self.motors_t)
			ksi_t = self.real_sensors_tp1 - self.world_out


			z = self.controler_step(self.sensors_t)
			G_der = self.diag((1 - TT.tanh(z)**2))
			L = TT.dot(TT.dot(self.world_weights,G_der),self.controler_weights)
			L += np.random.random((self.c_inp_s,self.c_inp_s)) * (10**-4)
			# rng.uniform((self.c_inp_s,self.c_inp_s)) * (10**-5)
			# np.random.random((self.c_inp_s,self.c_inp_s)) * (10**-5)
			L_inv = self.inverse(L)
			v = TT.dot(L_inv,ksi_t.flatten())

			xi = TT.dot(self.inverse(TT.dot(L,L.transpose())), ksi_t)
			TLE = TT.dot(ksi_t,xi).flatten()#TT.dot(xi.transpose(),ksi_t.dimshuffle(0,'x')).flatten()#(v**2).sum()
			miou = TT.dot(TT.dot(G_der,self.world_weights.transpose()),xi.flatten())
			zeta = TT.dot(self.controler_weights,v)
			eps = (2.*self.controler_lr* miou*zeta).flatten()

			eps = self.diag(eps.flatten())

			gControlerWeights, gCB = TT.grad(TLE[0], [self.controler_weights, self.controler_bias])

			gWorldWeights, gWB = TT.grad((ksi_t**2).sum()/self.w_out_s, [self.world_weights, self.world_bias])


			runner = theano.function(
				inputs=[
					self.sensors_t,
					self.motors_t,
					self.real_sensors_tp1,
					self.controler_lr,
					self.world_lr
				],
				# outputs=[v.transpose().shape,miou.shape ,xi.shape],
				# outputs=[L,G_der,z],
				outputs=[self.controler_weights,self.controler_bias, TLE/(ksi_t**2).sum()],
				# outputs=[TT.dot( self.controler_lr*miou,
				# 		v.transpose()
				# 	) -\
				# TT.dot( (eps*self.controler_out.dimshuffle((0,'x'))),
				# 		xi.transpose()
				# 	), (eps*self.controler_out.dimshuffle((0,'x'))).flatten(), v.transpose()],
				updates={
						(self.world_weights,
							self.world_weights - self.world_lr*gWorldWeights),
						(self.world_bias,
							self.world_bias - self.world_lr*gWB),
						(self.controler_weights,
							self.controler_weights +\
							(TT.dot( (self.controler_lr*miou).dimshuffle(0,'x'),
							 		v.dimshuffle('x',0)
								) -\
							TT.dot( TT.dot(eps,self.controler_out).dimshuffle(0,'x'),
									xi.dimshuffle('x',0)
								)
							)
							),
						(self.controler_bias,
							self.controler_bias - TT.dot(eps,self.controler_out)),
						# (self.controler_weights,
						# 	self.controler_weights - self.controler_lr * gControlerWeights),
						# (self.controler_bias,
						# 		self.controler_bias - self.controler_lr * gCB)
						},
				on_unused_input='ignore'
						)

			return runner
