#!/usr/bin/env python

import hm_minimal as hmkinesis

#import numpy
import numpy as np
from numpy import linalg as LA

import theano
import theano.tensor as TT

#import our tools
# from so_sml_v2.tools import sigmoid, dsigmoid

class Homeokinesis:
	def __init__(self, robot_sim, wlr, clr):

		#keep the robot
		self.robot_sim = robot_sim
		#get robots dimensions
		def getSensors():
			print "SENSOR ",robot_sim.getSensorByName("JointSensor")
			return robot_sim.getSensorByName("JointSensor")
			# return robot_sim.getSensorByName("JointVelocitySensor")
			# return robot_sim.getSensors()
		self.getSensors = getSensors
		w_out_size = len(self.getSensors())
		# w_out_size = len()
		# w_out_size = len()
		w_in_size = robot_sim.indim
		self.sensors = w_out_size
		self.actuators = w_in_size
		print "Number of Sensors: ", w_out_size, " Motors: ", w_in_size
		self.wlr = wlr
		self.clr = clr
		self.dt = 30
		#array for the sensory readings t-1
		self.senses = []
		#array for controllers actions t-1
		self.actions = []
		#array for predicted sensors t
		self.pred_senses = []

		'''
		self.hm_controllers = []
		#compile theano functions
		for i in xrange(robot_sim.indim):
			C = {}
			hmkn = hmkinesis.hm_run(w_out_size,1,1,w_out_size)
				# w_out_size,w_in_size,w_in_size,w_out_size)

			cs = hmkn.controler_step_f()
			ws = hmkn.world_step_f()
			rn = hmkn.run()
			C["cs"] = cs
			C["ws"] = ws
			C["rn"] = rn
			self.hm_controllers.append(C)

		def control_step(senses):
			response = [x["cs"](senses)[0][0] for x in self.hm_controllers]
			return np.array([response])

		def world_step(motors):
			response = np.zeros(w_out_size)
			for  mv_i,mv in enumerate(motors):
				for x in self.hm_controllers:
					response += x["ws"]([mv])[0]
					# print response.shape
				# response = [response+x["ws"]([mv])[0] for x in self.hm_controllers]
			response /= float(w_out_size)

			return np.array([response])

		def run_step(senses_tm2,actions_tm1,senses_tm1,clr,wlr):
			cw = 0
			ww = 0
			TLE = 0
			for c_i, c in enumerate(self.hm_controllers):
 			# 	import pdb; pdb.set_trace()
				cweights, wweights, tle = c["rn"](
								senses_tm2,[actions_tm1[c_i]],senses_tm1,clr,wlr)
				cw+=cweights
				ww+=wweights
				TLE+=tle
			return cw, ww, TLE

		self.controler_step = control_step
		self.world_step = world_step
		self.run = run_step
		'''

		hmkn = hmkinesis.hm_run(w_out_size,w_in_size,w_in_size,w_out_size)
		cs = hmkn.controler_step_f()
		ws = hmkn.world_step_f()
		rn = hmkn.run()

		self.controler_step = cs
		self.world_step = ws
		self.run = rn

		print "homeokinesis:: inited"

		pass



	def step(self, act=None):
		# import pdb; pdb.set_trace()
		#get readings
		self.senses.append(
				np.array(
					self.getSensors()
					# self.robot_sim.getSensors()
					# self.robot_sim.getSensorByName("JointSensor")
					# self.robot_sim.getSensorByName("JointVelocitySensor")
					)
			)

		# print self.robot_sim.getSensors()
		# .extend(
		# self.robot_sim.getSensorByName("JointVelocitySensor"))

		#make a step in the controller
		self.actions.append(
				self.controler_step(self.senses[-1])[0]
			)

		#perform action
		if act != None:
			self.robot_sim.performActionNew(
			act
			)
		else:
			# print "sasda d ", self.actions[-1]
			if len(self.actions) > 5:
				self.robot_sim.performActionNew(
				# np.ones_like(self.actions[-1])
				np.mean(self.actions[-5:],axis=0)
				)#*2-1)*950)
		# print " apply action, ",(self.actions[-1])

		self.robot_sim.step()


		#make a step in the world NN
		self.pred_senses.append(
			# np.array(
				self.world_step(self.actions[-1])[0]
				# )
			)


		#get readings
		# self.senses.append(
		# 	np.array(
		# 		self.getSensors()
		# 		# self.robot_sim.getSensors()
		# 		# self.robot_sim.getSensorByName("JointSensor")
		# 		# self.robot_sim.getSensorByName("JointVelocitySensor")
		# 		)
		# 	)


		# print "homeokinesis:: step sense:\n\t", self.senses[-2],\
			# "\n\taction:\n\t",self.actions[-1]," \n\tsense:\n\t", self.senses[-1]

		#return x_tm1, x_t, x_tp1, m_t
		if len(self.senses) > 3:
			return self.senses[-3], self.senses[-2], self.pred_senses[-1], np.mean(self.actions[-3:],axis=0)#self.actions[-1]
		else:
			return np.zeros(self.sensors),np.zeros(self.sensors),np.zeros(self.sensors),np.zeros(self.actuators)

	def learn(self,):
		if len(self.senses) < self.dt or len(self.actions) < self.dt:
			print "homeokinesis:: Not enough senses yet, length ", len(self.senses)
			return np.zeros((self.sensors, self.actuators)),np.zeros((self.actuators,self.sensors)),0

		# if self.actions[-1].all() == -1. or self.actions[-1].all() == 1.:
		# 	self.actions[-1] *=0.599
		# print self.senses[-2],self.actions[-1],	self.senses[-1]
		try:
			cweights, wweights, tle = self.run(
			self.senses[-self.dt],
			self.actions[-1],
			self.senses[-1],
			self.clr,
			self.wlr)
		except np.linalg.LinAlgError as e :
			raise e
		else:
			# print "homeokinesis:: learn\n\t cw \n\t",cweights,"\n\t ww \n\t",wweights
			return cweights, wweights, tle



'''
	def getJacobian(self, inp):
		'
			calculate the error as the Jacobian of the outputs
			and the inputs. The outputs of the world model
			and the inputs of the controller. This way we stabilize the
			whole network, and maintain a sqaure matrix for
			the Jacobian
			J = partial Wold_out_j / partial in_i
		'
		#normalize senses


		J = np.zeros((inp.shape[0],inp.shape[0]))
		for i in range(J.shape[0]):#going through the input size
			for j in range(J.shape[1]):#going through the output size
				the_sum = 0
				#all the weights from output i from world
				#self.w.weights[:,i]
				#all the weights form input j
				#self.c.weights[j,:]
				dv = []
				for ii in range(self.c.weights.shape[1]):
					d= self.c.df(np.sum(self.c.weights[:, ii] * inp))
					dv.append(d)
				for w_out, w_in,dvi in zip(self.w.weights[:,i],self.c.weights[j,:],dv):
					the_sum += w_out * w_in *dvi
				J[i,j] = the_sum

		return J
	def hm_run(self, c_inp_s, c_out_s, w_inp_s, w_out_s):
		#init variables
		controler_in = TT.vector(name="controler input")
		controler_out = TT.vector(name="controler output")
		controler_weights = theano.shared(
				np.random.uniform(0,1,(c_inp_s+1, c_out_s))
				)
		controler_input_vec = theano.shared(
				np.ones(c_inp_s+1).astype(theano.config.floatX)
				)
		controler_lr = TT.scalar(name="")
		controler_inp = TT.vector(name="")
		controler_out = TT.vector(name="")

		world_in = TT.vector(name="world input")
		world_out = TT.vector(name="world output")
		world_weights = theano.shared(
				np.random.uniform(0,1,(w_inp_s+1, w_out_s))
				)
		world_input_vec = theano.shared(
				np.ones(w_inp_s+1).astype(theano.config.floatX)
				)
		world_lr = TT.scalar(name="")
		world_inp = TT.vector(name="")
		world_out = TT.vector(name="")

		def controler_step(cin):
			controler_input_vec = TT.set_subtensor(
				controler_input_vec[:c_inp_s], cin
				)
			controler_outp =  TT.nnet.sigmoid(
				TT.dot(controler_input_vec, controler_weights)
				)

			return controler_outp
		def world_step(win):
			world_input_vec = TT.set_subtensor(
				world_input_vec[:w_inp_s], win
				)
			world_outp =  TT.dot(world_input_vec, world_weights)

			return world_outp

		#INPUTS
		sensors_t = TT.vector()
		motors_t = TT.vector()
		sensors_tp1 = TT.vector()
		# pred_sensors_tp1 = TT.vector()

		controler_step =  theano.function(
			inputs=[sensors_t],
			outputs=[controler_step(sensors_t)])

		world_step = theano.function(
			inputs=[motors_t],
			outputs=[world_step(motors_t)])

		cs = controler_step()
		ws = world_step()

		pred_sensors_tp1 = ws(cs(sensors_t))

		ksi_t = LA.norm(real_sensors_tp1 - pred_sensors_tp1)**2

		gWorldWeights = TT.grad(ksi_t, [world_weights])

		world_weights = world_weights - world_lr*gWorldWeights

		J = theano.gradient.jacobian(pred_sensors_tp1, sensors_t)
		L = LA.inv(J)
		u_t = L*ksi_t
		u_t = LA.norm(u_t)**2

		gControlerWeights = TT.grad(u_t, [controler_weights])

		controler_weights = controler_weights - controller_lr*gControlerWeights

		train = theano.function(
			inputs=[sensors_t,real_sensors_tp1],
			outputs=[])


'''
