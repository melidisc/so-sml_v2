__author__ = 'Christos Melidis, christos.melidis@plymouth.ac.uk'

from pybrain.rl.environments.ode import ODEEnvironment, sensors, actuators
import imp
from scipy import array
import numpy as np
import math

class ArmEnv(ODEEnvironment):
    def __init__(self, renderer=True, realtime=True, ip="127.0.0.1", port="21590", buf='16384'):
        ODEEnvironment.__init__(self, renderer, realtime, ip, port, buf)
        # load model file
        self.loadXODE(imp.find_module('pybrain')[1] + "/rl/environments/ode/models/chrismodel.xode")
        # standard sensors and actuators
        self.addSensor(sensors.JointSensor())
        self.addSensor(sensors.JointVelocitySensor())
        # self.addSensor(sensors.DistToPointSensor("body"))
        self.addActuator(actuators.JointActuator())
        # self.FricMu = .010
        self.dt = 0.02
        # self.setGravity(0.5)
        #set act- and obsLength, the min/max angles and the relative max touques of the joints
        self.actLen = self.indim
        self.obsLen = len(self.getSensors())
        self.maxPower = 400.0

        self.tourqueList = np.array([0.2, 0.2, 0.2, 0.5, 0.5, 2.0, 2.0, 2.0, 2.0, 0.5, 0.5])
        self.cHighList = np.array([1.0, 1.0, 0.5, 0.5, 0.5, 1.5, 1.5, 1.5, 1.5, 0.25, 0.25])
        self.cLowList = np.array([-0.5, -0.5, -0.5, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -0.25, -0.25])
        # self.cHighList = np.array([1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0])
        # self.cLowList = np.array([-.5, -.5, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0])
        self.mgnt = np.array([25,  25,  5,   5,    5,    10,   10,    10,    10,   10,    10])

        self.sensor_limits = []
        #Angle sensors
        for i in range(self.actLen):
            self.sensor_limits.append((self.cLowList[i], self.cHighList[i]))
        # Joint velocity sensors
        for i in range(self.actLen):
            self.sensor_limits.append((-20., 20.))

        # self.tourqueList = np.array([400,400])
        # self.cHighList = np.array([3.14,3.14])
        # self.cLowList = np.array([-3.14,-3.14])
        # self.mgnt = np.array([2,  2])

        self.stepsPerAction = 1

    def getObsLen(self):
        return self.obsLen
    def performActionNew(self,action, reshape_func=None):
        #Filtered mapping towards performAction of the underlying environment
        #The standard Johnnie task uses a PID controller to controll directly angles instead of forces
        #This makes most tasks much simpler to learn

        isJoints=self.getSensorByName('JointSensor') #The joint angles
        isSpeeds=self.getSensorByName('JointVelocitySensor') #The joint angular velocitys
        act=(action+1.0)/2.0*(self.cHighList-self.cLowList)+self.cLowList #norm output to action intervall
        action=np.tanh((act-isJoints-isSpeeds)*16.0)*self.maxPower*self.tourqueList #simple PID

        # action[0] *= np.random.normal(1., 0.1)
        # action[1] *= np.random.normal(1., 0.1)
        # action = action * np.random.normal(1.,.25,len(action/2+1))
        # print "action %s" %(action)

        # action = np.clip(action, -1*self.tourqueList, self.tourqueList)
        if np.isnan(np.min(action)):
            return 0
        # print action
        # action+=1
        # action/=2
        # action*=(self.cHighList-self.cLowList)
        # action+=self.cLowList
        # print action
        # self.performAction(act*self.maxPower)
        self.performAction(action*self.mgnt)
        # self.performAction(np.append(action[:3]*1.3,action[3:]))
if __name__ == '__main__' :
    w = OctacrawlEnv()
    while True:
        act = 2*np.random.random(2)-1
        w.performAction(750*act)
        print w.getSensors()
        w.step()
        if w.stepCounter == 1000: w.reset()
