__author__ = 'Christos Melidis, christos.melidis@plymouth.ac.uk'

from pybrain.rl.environments.ode import ODEEnvironment, sensors, actuators
import imp
from scipy import array
import numpy as np
import math

class BallEnv(ODEEnvironment):
    def __init__(self, renderer=True, realtime=True, ip="127.0.0.1",
                port="21590", buf='16384'):
        ODEEnvironment.__init__(self, renderer, realtime, ip, port, buf)
        # load model file
        self.loadXODE(imp.find_module('pybrain')[1] + "/rl/environments/ode/models/ballmodel.xode")
        # standard sensors and actuators
        self.addSensor(sensors.JointSensor())
        self.addSensor(sensors.JointVelocitySensor())
        # self.addSensor(sensors.DistToPointSensor("body"))
        acts = actuators.JointActuator()
        self.addActuator(acts)
        self.dt = 0.035
        #set act- and obsLength, the min/max angles and the relative max touques of the joints
        self.actLen = self.indim
        self.obsLen = len(self.getSensorByName("JointSensor"))

        # self.tourqueList = np.array([.5,.5,.5,.5,.25,.1,.5,.25,.1,.5,.25,.1,.5,.25,.1])
        # self.cHighList = np.array([2,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5])
        # self.cLowList = np.array([-2.,-1.5,-1.5,.0,.0,.0,.0,.0,.0,.0,.0,.0,.0,.0,.0])
        # self.mgnt = np.array([1000,  500,  100,   10,    1,    1,   10,    1,    1,   10,    1,  1,   10,    1,    1])

        self.tourqueList = np.array([5.]*self.obsLen)
        self.cHighList = np.array([1]*self.obsLen)
        self.cLowList = np.array([-1]*self.obsLen)
        self.mgnt = np.array([1.]*self.obsLen)

        self.stepsPerAction = 1

    def getObsLen(self):
        return self.obsLen
    def performActionNew(self,action, reshape_func=None):
        #Filtered mapping towards performAction of the underlying environment
        #The standard Johnnie task uses a PID controller to controll directly angles instead of forces
        #This makes most tasks much simpler to learn
        if reshape_func != None:
            isJoints=reshape_func(self.getSensorByName('JointSensor')) #The joint angles
            isSpeeds=reshape_func(self.getSensorByName('JointVelocitySensor')) #The joint angular velocitys
        else:
            isJoints=self.getSensorByName('JointSensor') #The joint angles
            isSpeeds=self.getSensorByName('JointVelocitySensor') #The joint angular velocitys
        act=(action+1.0)/2.0*(self.cHighList-self.cLowList)+self.cLowList #norm output to action intervall
        action=np.tanh((act-isJoints)*2.0)*self.mgnt*self.tourqueList #simple PID

        # action[0] *= np.random.normal(1., 0.1)
        # action[1] *= np.random.normal(1., 0.1)
        # action = action * np.random.normal(1.,.25,len(action/2+1))
        # print "action %s" %(action)
        # test = np.array([1.0,1.0])
        # action = test
        # futurePos = isJoints
        # desiredPos = action
        # applyForce = (futurePos - desiredPos) * 1.
        # if (abs(applyForce) > self.tourqueList).all():
        #     applyForce = self.tourqueList
        # action = applyForce
        # action = np.clip(action, -1*self.tourqueList, self.tourqueList)
        if np.isnan(np.min(action)):
            return 0
        # print test*self.tourqueList
        print action#ion,applyForce
        # self.performAction(action)#*self.cHighList)#np.append(action[:3],action[3:]*0.4))
        self.performAction(action)#*self.tourqueList)

        # self.performAction(test*self.tourqueList)
if __name__ == '__main__' :
    w = OctacrawlEnv()
    while True:
        act = 2*np.random.random(2)-1
        w.performAction(750*act)
        print w.getSensors()
        w.step()
        if w.stepCounter == 1000: w.reset()
