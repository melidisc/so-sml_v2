__author__ = 'Christos Melidis, christos.melidis@plymouth.ac.uk'

from pybrain.rl.environments.ode import ODEEnvironment, sensors, actuators
import imp
from scipy import array
import numpy as np
import math
import pdb, itertools

class SphereWalkerEnv(ODEEnvironment):
    def __init__(self, renderer=True, realtime=True, ip="127.0.0.1", port="21590", buf='16384'):
        ODEEnvironment.__init__(self, renderer, realtime, ip, port, buf)
        # load model file
        # pdb.set_trace()
        self.loadXODE(imp.find_module('pybrain')[1] + "/rl/environments/ode/models/sphere-walker.xode")
        # standard sensors and actuators
        # pdb.set_trace()
        self.addSensor(sensors.JointSensor())
        self.addSensor(sensors.JointVelocitySensor())
        acts = actuators.JointActuator()
        self.addActuator(acts)
        self.FricMu = 30.
        self.dt = 0.034
        #set act- and obsLength, the min/max angles and the relative max touques of the joints
        self.actLen = self.indim
        self.obsLen = len(self.getSensors())

        # self.tourqueList = np.array([.5,.5,.5,.5,.25,.1,.5,.25,.1,.5,.25,.1,.5,.25,.1])
        # self.cHighList = np.array([2,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5])
        # self.cLowList = np.array([-2.,-1.5,-1.5,.0,.0,.0,.0,.0,.0,.0,.0,.0,.0,.0,.0])
        # self.mgnt = np.array([1000,  500,  100,   10,    1,    1,   10,    1,    1,   10,    1,  1,   10,    1,    1])
        self.cHighList = array([1.,1.])
        self.cLowList = array([-1.,-1.])
        self.tourqueList = np.array([50.,50.])*100.
        # self.mgnt = 200.0
        self.sensor_limits = []
        # Angle sensors

        for i in range(self.actLen):
            # Joint velocity sensors
            self.sensor_limits.append((self.cLowList[i], self.cHighList[i]))

        for i in range(self.actLen):
            self.sensor_limits.append((-1, 1))

        self.actor_limits = [(-100., 100.)]*self.actLen

        self.stepsPerAction = 1
        self.I = .0
        self.D = 1.0
        self.I_max = 10#self.tourqueList
        self.I_min = -10#*self.tourqueList

    def getObsLen(self):
        return self.obsLen
    def performActionNew(self,action, reshape_func=None):
        #Filtered mapping towards performAction of the underlying environment
        #The standard Johnnie task uses a PID controller to controll directly angles instead of forces
        #This makes most tasks much simpler to learn

        isJoints=self.getSensorByName('JointSensor') #The joint angles
        isSpeeds=self.getSensorByName('JointVelocitySensor') #The joint angular velocitys

        # dif = np.tanh(action-isJoints)*2.
        # for i in range(len(isSpeeds)):
        #     if np.absolute(isSpeeds[i]) < 0.5:
        #         dif[i] *= 70.
        #
        # action = dif

        # error = action-isJoints#*self.tourqueList
        # P = .1#.5*16.0
        # self.I +=  error
        # for i in self.I:
        #     if i > self.I_max:
        #         i = self.I_max
        #     elif i < self.I_min:
        #         i = self.I_min
        # I_v = self.I * .10
        # D_v = self.D * (error - self.D)
        # action = P+I_v+D_v

        # act=(action+1.0)/2.0*(self.cHighList-self.cLowList)+self.cLowList
        # action=np.tanh((act-isJoints-isSpeeds)*3.0)*self.tourqueList #simple PID
        # action = np.tanh(action-isJoints)*1.5*self.tourqueList
        action = action*self.tourqueList
        print "ACTION ", action

        self.performAction(action)#*self.tourqueList)

if __name__ == '__main__' :
    w = SphereWalkerEnv()
    linspace = np.append(np.linspace(np.pi/6, 5*np.pi/6, 40),np.linspace(7*np.pi/6, 11*np.pi/6,40))
    linspace_reverse = np.append(np.linspace(-np.pi/6, -5*np.pi/6, 40),np.linspace(-7*np.pi/6, -11*np.pi/6,40))
    sin_input = ((np.sin(x),np.sin(y)) for x,y in itertools.cycle(zip(linspace, linspace_reverse)))
    while True:
        # if (w.stepCounter % 30) == 0:
        act = sin_input.next()
        # act += 0.2*np.random.random(2)-.1
        # print act
        w.performAction(30.*np.array(act))
        print act,"\r"
        print w.getSensors()
        w.step()
        # if w.stepCounter == 100000: w.reset()
