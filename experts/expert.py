import numpy as np
from numpy import linalg as LA

import theano
import theano.tensor as TT

class Expert():
	def __init__(self, indim,outdim,hdim):
		self.inp_s = indim
		self.out_s = outdim
		self.hid_s = hdim

		self.weights_ih = theano.shared(
			np.random.uniform(
				-1,
				1,
				(self.inp_s+1, self.hid_s)
				).astype(theano.config.floatX)
			)

		self.weights_ho = theano.shared(
			np.random.uniform(
				-1,
				1,
				(self.hid_s, self.out_s)
				).astype(theano.config.floatX)
			)

		self.inp_vec = theano.shared(
			np.ones(self.inp_s+1).astype(theano.config.floatX)
			)
		self.lr = TT.scalar(name="learning rate")
		self.inp = TT.vector(name="input")
		self.target = TT.vector(name="target")


	def step(self, vin):
		inp = TT.set_subtensor(
				self.inp_vec[:self.inp_s], vin
				)

		ha = TT.nnet.sigmoid(
			TT.dot(inp,self.weights_ih)
			)
		oa = TT.dot(ha,self.weights_ho)

		return oa
	def step_f(self,):
		output = self.step(self.inp)

		return theano.function(
			inputs=[self.inp],
			outputs=[output])
	def train_f(self,):

		output = self.step(self.inp)

		E = ((self.target - output)**2).sum()

		gW_ih, gW_ho = TT.grad(E, [self.weights_ih, self.weights_ho])



		return theano.function(
			inputs=[self.inp, self.target, self.lr],
			outputs=[E, output],
			updates=[
			(self.weights_ih, self.weights_ih - self.lr*gW_ih),
			(self.weights_ho, self.weights_ho - self.lr*gW_ho)
			]
			)

class Experts():
	def __init__(self, num_exp, ind, outd, hd):
		self.experts = []
		self.max_db_el = 300
		for i in xrange(num_exp):
			ex = Expert(
				ind,
				outd,
				hd
				)
			self.experts.append({
				"step": ex.step_f(),
				"train": ex.train_f(),
				"dtset":[],
				"idx":0
				}
				)




	def step(self, inp, nExp=None):
		if nExp == None:
			activations = []
			for ex in self.experts:
				activations.append(
					ex["step"](inp)
					)
			return activations
		else:
			return self.experts[nExp]["step"](inp)

	def train(self, inp, t_out, ex_lr):
		pred =[]

		for ex in self.experts:
			pred_out = ex["step"](inp)
			error =  ((t_out-pred_out)**2).sum()
			pred.append(error)

		best_one = np.argmin(pred)
		#train the best
		if len(self.experts[best_one]["dtset"]) < 300:
			self.experts[best_one]["dtset"].append((inp, t_out))
			self.experts[best_one]["idx"]+=1
		else:
			get_id = self.experts[best_one]["idx"] % 300
			self.experts[best_one]["dtset"][get_id]=(inp, t_out)

		for dt_el in self.experts[best_one]["dtset"]:
			error, outpu = self.experts[best_one]["train"](dt_el[0], dt_el[1], ex_lr)
		# for best_one in xrange(len(self.experts)):
		# 	for dt_el in self.experts[best_one]["dtset"]:
		# 		error, outpu = self.experts[best_one]["train"](dt_el[0], dt_el[1], ex_lr)
		# print "winner %s with error %s \n\t E out %s, real out %s"%(
		# 		best_one, error, outpu, t_out)
		print best_one,pred

	def save(self, filename):
		import pickle

		f= open("experts/saves/"+str(filename)+".pickle","wb")

		pickle.dump(self, f)

		f.close()

		return True
	def load(self, filename):
		import pickle

		f=open("experts/saves/"+str(filename)+".pickle","rb")

		self = pickle.load(f)

		f.close()

		return True




if __name__ =="__main__":

	e = Expert(5,5,3)
	step = e.step_f()
	train = e.train_f()

	inp = np.random.random(5)
	output = step(inp)
	print output

	target = np.random.random(5)

	error, output = train(inp, target, 0.1)

	print error, target

	inp = np.random.random(5)
	output = step(inp)

	print output
